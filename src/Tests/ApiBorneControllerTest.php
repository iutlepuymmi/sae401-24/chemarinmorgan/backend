<?PHP

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiBorneControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();

        $client->request('GET', '/api/borne');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertTrue(
            $client->getResponse()->headers->contains('Content-Type', 'application/json'),
            'La réponse doit être du JSON'
        );

        $client->request('GET', '/api/borne', ['page' => 2, 'perPage' => 10]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(10, $data);
    }
}