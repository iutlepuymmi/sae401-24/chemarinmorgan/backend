<?php

namespace App\Entity;

use App\Repository\BorneRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BorneRepository::class)]
class Borne
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $operateur = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $adresse = null;

    #[ORM\Column(nullable: true)]
    private ?int $code_commune = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $coordonnees_xy = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $date_mise_en_service = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $nom_commune = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOperateur(): ?string
    {
        return $this->operateur;
    }

    public function setOperateur(?string $operateur): static
    {
        $this->operateur = $operateur;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): static
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCodeCommune(): ?int
    {
        return $this->code_commune;
    }

    public function setCodeCommune(?int $code_commune): static
    {
        $this->code_commune = $code_commune;

        return $this;
    }

    public function getCoordonneesXy(): ?string
    {
        return $this->coordonnees_xy;
    }

    public function setCoordonneesXy(?string $coordonnees_xy): static
    {
        $this->coordonnees_xy = $coordonnees_xy;

        return $this;
    }

    public function getDateMiseEnService(): ?string
    {
        return $this->date_mise_en_service;
    }

    public function setDateMiseEnService(?string $date_mise_en_service): static
    {
        $this->date_mise_en_service = $date_mise_en_service;

        return $this;
    }

    public function getNomCommune(): ?string
    {
        return $this->nom_commune;
    }

    public function setNomCommune(?string $nom_commune): static
    {
        $this->nom_commune = $nom_commune;

        return $this;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'operateur' => $this->getOperateur(),
            'adresse' => $this->getAdresse(),
            'codecommunec' => $this->getCodeCommune(),
            'coordonnees' => $this->getCoordonneesXY(),
            'datemiseenservice' => $this->getDateMiseEnService(),
            'nomcommune' => $this->getNomCommune(),
        ];

    }
}
