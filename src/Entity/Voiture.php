<?php

namespace App\Entity;

use App\Repository\VoitureRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VoitureRepository::class)]
class Voiture
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    private ?int $codgeo = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $libgeo = null;

    #[ORM\Column(nullable: true)]
    private ?int $vp_elec = null;

    #[ORM\Column(nullable: true)]
    private ?int $vp = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodgeo(): ?int
    {
        return $this->codgeo;
    }

    public function setCodgeo(?int $codgeo): static
    {
        $this->codgeo = $codgeo;

        return $this;
    }

    public function getLibgeo(): ?string
    {
        return $this->libgeo;
    }

    public function setLibgeo(?string $libgeo): static
    {
        $this->libgeo = $libgeo;

        return $this;
    }

    public function getVpElec(): ?int
    {
        return $this->vp_elec;
    }

    public function setVpElec(?int $vp_elec): static
    {
        $this->vp_elec = $vp_elec;

        return $this;
    }

    public function getVp(): ?int
    {
        return $this->vp;
    }

    public function setVp(?int $vp): static
    {
        $this->vp = $vp;

        return $this;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'codegeo' => $this->getCodgeo(),
            'libgeo' => $this->getLibgeo(),
            'vpElec' => $this->getVpElec(),
            'vp' => $this->getVp(),
        ];

    }
}
