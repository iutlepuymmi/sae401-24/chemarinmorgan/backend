<?php

namespace App\Controller;

use App\Entity\Borne;
use App\Form\BorneType;
use App\Repository\BorneRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/admin/borne')]
class BorneController extends AbstractController
{
    #[Route('/', name: 'app_borne_index', methods: ['GET'])]
    public function index(BorneRepository $borneRepository): Response
    {
        return $this->render('borne/index.html.twig', [
            'bornes' => $borneRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_borne_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $borne = new Borne();
        $form = $this->createForm(BorneType::class, $borne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($borne);
            $entityManager->flush();

            return $this->redirectToRoute('app_borne_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('borne/new.html.twig', [
            'borne' => $borne,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_borne_show', methods: ['GET'])]
    public function show(Borne $borne): Response
    {
        return $this->render('borne/show.html.twig', [
            'borne' => $borne,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_borne_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Borne $borne, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(BorneType::class, $borne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_borne_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('borne/edit.html.twig', [
            'borne' => $borne,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_borne_delete', methods: ['POST'])]
    public function delete(Request $request, Borne $borne, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$borne->getId(), $request->request->get('_token'))) {
            $entityManager->remove($borne);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_borne_index', [], Response::HTTP_SEE_OTHER);
    }
}
