<?php

namespace App\Controller;


use App\Entity\Borne;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiBorneController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/api/borne', name: 'app_api_borne')]
    public function index(Request $request): Response
    {
        $page = $request->query->getInt('page', 1);
        $parPage = $request->query->getInt('perPage', 5000);
        $offset = ($page - 1) * $parPage;

        $databornes = $this->entityManager->getRepository(Borne::class)
            ->findBy([], null, $parPage, $offset);

        $databorneArray = [];

        $databorneArray = [];
        foreach ($databornes as $databorne) {
            $databorneArray[] = $databorne->toArray();
        }


        return $this->json($databorneArray, 200, [], ['charset' => 'utf-8']);

    }
}
