<?php

namespace App\Controller;

use App\Entity\Voiture;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiVoitureController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/api/voiture', name: 'app_api_voiture')]
    public function index(Request $request): Response
    {
        $page = $request->query->getInt('page', 1);
        $parPage = $request->query->getInt('parPage', 5000);
        $offset = ($page - 1) * $parPage;

        $datavoitures = $this->entityManager->getRepository(Voiture::class)
            ->findBy([], null, $parPage, $offset);

        $datavoitureArray = [];

        $datavoitureArray = [];
        foreach ($datavoitures as $datavoiture) {
            $datavoitureArray[] = $datavoiture->toArray();
        }


        return $this->json($datavoitureArray, 200, [], ['charset' => 'utf-8']);

    }
}