<?php

namespace App\Form;

use App\Entity\Borne;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BorneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('operateur')
            ->add('adresse')
            ->add('code_commune')
            ->add('coordonnees_xy')
            ->add('date_mise_en_service')
            ->add('nom_commune')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Borne::class,
        ]);
    }
}
