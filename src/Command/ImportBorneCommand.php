<?php

namespace App\Command;

use App\Entity\Borne;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

ini_set('memory_limit', '1024M');

#[AsCommand(
    name: 'app:import-borne',
    description: 'Importe des données depuis un fichier CSV BorneLoire.csv dans la base de données.'
)]
class ImportBorneCommand extends Command
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Importe des données depuis un fichier CSV dans la base de données.')
            ->addArgument('csvFile', InputArgument::REQUIRED, 'Le chemin vers le fichier CSV');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $csvFile = $input->getArgument('csvFile');
        if (!file_exists($csvFile) || !is_readable($csvFile)) {
            $output->writeln('Fichier CSV introuvable ou non lisible.');
            return Command::FAILURE;
        }

        $handle = fopen($csvFile, 'r');
        stream_filter_append($handle, 'convert.iconv.UTF-8/UTF-8//IGNORE');
        $batchSize = 20;
        $i = 0;

        while (($row = fgetcsv($handle, 0, ";")) !== FALSE) {
            if ($i == 0) {
                $i++;
                continue;
            }

            if (!mb_check_encoding($row[1], 'UTF-8')) {
                $row[1] = mb_convert_encoding($row[1], 'UTF-8', mb_detect_encoding($row[1], 'UTF-8, ISO-8859-1, Windows-1252', true));

            }


            if (count($row)) {
                $entity = new Borne();

                $entity->setOperateur($row[0]);
                $entity->setAdresse($row[1]);
                $entity->setCodeCommune((int) $row[2]);
                $entity->setCoordonneesXY($row[3]);
                $entity->setDateMiseEnService($row[4]);
                $entity->setNomCommune($row[8]);

                $this->entityManager->persist($entity);

                if (($i % $batchSize) === 0) {
                    $this->entityManager->flush();
                    $this->entityManager->clear();
                }
                $i++;
            } else {
                $output->writeln("Ligne $i ignorée car elle ne contient pas le bon nombre de colonnes.");
                $i++;
            }
        }

        fclose($handle);

        if ($i > 1) {
            $this->entityManager->flush();
            $this->entityManager->clear();
            $output->writeln('Importation réussie.');
            return Command::SUCCESS;
        } else {
            $output->writeln('Aucune donnée importée.');
            return Command::FAILURE;
        }
    }

}
