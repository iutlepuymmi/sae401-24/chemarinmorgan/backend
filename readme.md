# SAE 401 : borneElec
## Partie backend - symfony 

Pour ce projet, j'ai choisi deux jeux de données, les bornes éléctriques et les voitures immatriculées par commune. Le nombre de données étant trop grand, j'ai choisi de traiter les données seulement dans la Loire (42).

Du côté backend, je trasnplante les fichiers csv de base dans des tables issus d'une base de données géré par doctrine : [Fichier Command](./src/Command/ImportBorneCommand.php)

Ensuite, le [Controller ApiBorneController](./src/Controller/ApiBorneController.php), retourne les données convertis en json de la table Borne.

Pour être restfull, j'ai ajouté des cruds de chaque table : [Borne](/src/Entity/Borne.php) , [Voiture](/src/Entity/Voiture.php) et [User](/src/Entity/User.php) qui permette d'ajouter, modifier ou supprimer des données précises dans chaque table.

Dans chaque table, on retrouve cette fonction qui permet de mettre en forme le JSON renvoyé par les Controlles API : 
```php
public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'operateur' => $this->getOperateur(),
            'adresse' => $this->getAdresse(),
            'codecommunec' => $this->getCodeCommune(),
            'coordonnees' => $this->getCoordonneesXY(),
            'datemiseenservice' => $this->getDateMiseEnService(),
            'nomcommune' => $this->getNomCommune(),
        ];

    }
```

Il y a aussi un systeme de chemin sécurisé grâce au rôle ROLES_ADMIN.

## Fonctionnement backend

Le backend est accessible à l'url : [ici](https://backendmc.mmidev.oktopod.app/)
On arrive directement sur une page login. 

Pour etre admin : 
pseudo : morgan
mdp : test12

Ensuite, il est alors possible d'aller sur les cruds.

Les API sont ici :
- https://backendmc.mmidev.oktopod.app/api/borne
- https://backendmc.mmidev.oktopod.app/api/voiture

## Test unitaire

Les test unitaires des API se trouvent dans ce dossier :  [ici](./src/Tests/Controller/)